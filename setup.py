from setuptools import setup, find_packages
import os

current_dir = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(current_dir, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
     name = 'LRT-util',
     version = '0.0.13',
     description = 'Use libRadtran to calculate radiative properties, e.g., radiance, irradiance etc.',
     long_description = long_description,
     classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
        ],
     keywords = 'libRadtran radiation model RTM',
     url = 'https://github.com/hong-chen/libradtran-util',
     author = 'Hong Chen',
     author_email = 'me@hongchen.cz',
     license = 'MIT',
     packages = find_packages(),
     install_requires = ['nose', 'numpy', 'scipy'],
     python_requires = '~=3.6',
     include_package_data = True,
     zip_safe = False
     )
