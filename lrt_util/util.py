import numpy as np



__all__ = ['gen_cloud_1d', 'gen_aerosol_1d', 'gen_wavelength_file', 'gen_surface_albedo_file']



def gen_cloud_1d(cld_cfg):

    altitude = cld_cfg['cloud_altitude']

    alt = np.sort(altitude)[::-1]
    lwc = np.zeros_like(alt)
    cer = np.zeros_like(alt)

    lwc[1:] = cld_cfg['liquid_water_content']
    cer[1:] = cld_cfg['cloud_effective_radius']

    with open(cld_cfg['cloud_file'], 'w') as f:
        f.write('# Altitude[km]    Liquid Water Content [g/m3]    Cloud Effective Radius [um]\n')
        for i, alt0 in enumerate(altitude):
            f.write('     %.2f                   %.2f                          %.2f\n' % (alt[i], lwc[i], cer[i]))
        if abs(alt[-1]-0.0)>0.001:
            f.write('     %.2f                   %.2f                          %.2f\n' % (0.0, 0.0, 0.0))



def gen_aerosol_1d(aer_cfg):

    altitude = aer_cfg['aerosol_altitude']

    alt = np.sort(altitude)[::-1]
    aod = np.zeros_like(alt)

    aod[1:] = aer_cfg['aerosol_optical_depth']

    with open(aer_cfg['aerosol_file'], 'w') as f:
        f.write('# Altitude[km]   Aerosol Optical Depth\n')
        for i, alt0 in enumerate(altitude):
            f.write('     %.2f                %.2f\n' % (alt[i], aod[i]))
        if abs(alt[-1]-0.0)>0.001:
            f.write('     %.2f                %.2f\n' % (0.0, 0.0))



def gen_wavelength_file(fname_wvl, wvls):

    np.savetxt(fname_wvl, wvls, fmt='%.3f')



def gen_surface_albedo_file(fname_alb, wvls, albs):

    np.savetxt(fname_alb, np.column_stack((wvls, albs)), fmt='%.3f')



if __name__ == '__main__':

    pass
