import os
import numpy as np
import datetime
import lrt_util as lrt



def eg_01_clear_sky_default():

    """
    This following is an example for clear-sky calculation with default parameterization.
    """

    init = lrt.lrt_init_mono()
    lrt.lrt_run(init)

    data = lrt.lrt_read_uvspec([init])

    # the flux calculated can be accessed through
    print(data.f_up)
    print(data.f_down)
    print(data.f_down_diffuse)
    print(data.f_down_direct)



def eg_02_clear_sky_user_specified():

    """
    The following example is similar to Example 1 but with user's input of surface albedo, solar zenith angle,
    wavelength etc.
    """

    lrt_cfg = lrt.get_lrt_cfg()
    lrt_cfg['atmosphere_file'] = lrt_cfg['atmosphere_file'].replace('afglus.dat', 'afglss.dat')

    init = lrt.lrt_init_mono(
            input_file  = 'input.txt',
            output_file = 'output.txt',
            date        = datetime.datetime(2014, 9, 11),
            surface_albedo     = 0.8,
            solar_zenith_angle = 70.0,
            wavelength         = 532.31281,
            output_altitude    = 5.0,
            lrt_cfg            = lrt_cfg,
            )
    lrt.lrt_run(init)

    data = lrt.lrt_read_uvspec([init])

    # the flux calculated can be accessed through
    print(data.f_up)
    print(data.f_down)
    print(data.f_down_diffuse)
    print(data.f_down_direct)



def eg_03_clear_sky_multiple():

    """
    The following example is similar to Example 2 but for multiple calculations at different solar zenith angles.
    """

    sza = np.arange(60.0, 65.1, 0.5)

    lrt_cfg = lrt.get_lrt_cfg()
    lrt_cfg['atmosphere_file'] = lrt_cfg['atmosphere_file'].replace('afglus.dat', 'afglss.dat')

    inits = []
    for i, sza0 in enumerate(sza):
        init = lrt.lrt_init_mono(
                input_file  = 'input%2.2d.txt' % i,
                output_file = 'output%2.2d.txt'% i,
                date        = datetime.datetime(2014, 9, 11),
                surface_albedo     = 0.8,
                solar_zenith_angle = sza0,
                wavelength         = 532.31281,
                output_altitude    = 5.0,
                lrt_cfg            = lrt_cfg
                )
        inits.append(init)

    # run with multi cores
    lrt.lrt_run_mp(inits, ncpu=6)

    data = lrt.lrt_read_uvspec(inits)

    # the flux calculated can be accessed through
    print(data.f_up)
    print(data.f_down)
    print(data.f_down_diffuse)
    print(data.f_down_direct)



def eg_04_cloudy_multiple():

    """
    The following example is similar to Example 3 but for cloud calculations.
    Assume we have a homogeneous cloud layer (COT=10.0, CER=12.0) located at 0.5 to 1.0 km.
    """

    sza = np.arange(60.0, 65.1, 0.5)

    lrt_cfg = lrt.get_lrt_cfg()
    lrt_cfg['atmosphere_file'] = lrt_cfg['atmosphere_file'].replace('afglus.dat', 'afglss.dat')

    cld_cfg = lrt.get_cld_cfg()
    cld_cfg['cloud_optical_thickness'] = 10.0
    cld_cfg['cloud_effective_radius']  = 12.0
    cld_cfg['cloud_altitude']  = np.arange(0.5, 1.1, 0.1)
    cld_cfg['cloud_file']      = 'cloud.txt'

    inits = []
    for i, sza0 in enumerate(sza):

        init = lrt.lrt_init_mono(
                input_file  = 'input%2.2d.txt' % i,
                output_file = 'output%2.2d.txt'% i,
                date        = datetime.datetime(2014, 9, 11),
                surface_albedo     = 0.8,
                solar_zenith_angle = sza0,
                wavelength         = 532.31281,
                output_altitude    = 5.0,
                lrt_cfg            = lrt_cfg,
                cld_cfg            = cld_cfg
                )
        inits.append(init)

    # run with multi cores
    lrt.lrt_run_mp(inits, ncpu=6)

    data = lrt.lrt_read_uvspec(inits)

    # the flux calculated can be accessed through
    print(data.f_up)
    print(data.f_down)
    print(data.f_down_diffuse)
    print(data.f_down_direct)



if __name__ == '__main__':

    # eg_01_clear_sky_default()

    # eg_02_clear_sky_user_specified()

    # eg_03_clear_sky_multiple()

    # eg_04_cloudy_multiple()
