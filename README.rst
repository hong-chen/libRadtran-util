LRT-util
~~~~~~~~




===========
Description
===========

In this Python wrapper code, the configurations for :literal:`libRadtran` itself,
:literal:`clouds`, and :literal:`aerosols` are defined using Python dictionaries:

- :literal:`lrt_cfg` for libRadtran;
- :literal:`cld_cfg` for clouds;
- :literal:`aer_cfg` for aerosols.

These configuration dictionaries (if have) will be passed to the
class :literal:`lrt_init*` to create an object instance. Then
the function :literal:`lrt_run*` will take this object instance as input and
create a libRadtran input file and run the libRadtran executable
to create an output file. The object instance can also be used as input for
class :literal:`lrt_read_uvspec` to extract the output fluxes. Examples will be provided
in the following section.

:literal:`multiprocessing` is used in function :literal:`lrt_run_mp` with
6 cores as default.

|




==========
How to Use
==========

Examples can be found `here <https://github.com/hong-chen/libRadtran-util/blob/master/examples/example.py>`_.

|




=======================
How to Install (Part 1)
=======================

You need to have `libRadtran <http://www.libradtran.org>`_ downloaded and compiled.

You can find detailed instructions of libRadtran installation on Mac `here <https://github.com/hong-chen/notes/blob/master/libRadtran.md>`_.

|





=======================
How to Install (Part 2)
=======================

1. If you have already installed using

::

    pip install LRT-util

Please uninstall it through

::

    pip uninstall LRT-util


2. Open a terminal, type in the following

::

    git clone https://github.com/hong-chen/libRadtran-util.git


3. Under newly cloned ``libRadtran-util``, where it contains ``setup.py``, type in the following

::

    python setup.py develop


4. After installation, add the following to the source file of the shell you use:

If you are using bash shell, edit ``.bash_profile`` (Mac) or ``.bashrc`` (Linux)

::

    export LIBRADTRAN_PY="/path/to/the/directory/of/libradtran"

If you are using c shell, edit ``.cshrc``

::

    setenv LIBRADTRAN_PY "/path/to/the/directory/of/libradtran"


|




=====
F.A.Q
=====

1. How to update local ``libRadtran-util`` repository?

::

    git checkout master
    git pull origin master

    python setup.py develop


2. What to do if encounter conficts in file change when ``git pull``?

::

    git checkout master
    git fetch --all
    git reset --hard origin/master
    git pull origin master

    python setup.py develop
